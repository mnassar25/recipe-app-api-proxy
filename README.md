# recipe-app-api-proxy

NGINX proxy app for our receipe app API

## USage

### Environment Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the App to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)